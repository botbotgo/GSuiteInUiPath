## Google GSuite Activities in UiPath

The Google GSuite Activities Package helps you automate Google Cloud G Suite applications, including Google Calendar, Google Drive, Google Sheets, Gmail, and Google Docs.

With the Google GSuite Activities Package, you can create & modify Google Calendar events, manage Google Drive files, read & send GMail messages, create new Google Sheet spreadsheets, and Google Docs documents..

### Before running the workflow

1. Install UIPATH.GSUITE.ACTIVITIES
2. Configure your Application In [Google Cloud Platform](https://console.cloud.google.com/) to get ClientID and ClientSecret. Here's the link to [detailed video](https://www.youtube.com/watch?v=GB7s0XGUQKg&list=PL0ugL3aVdd-lCzn0rCKgtL-F6ECRM_N96&index=2&t=0s).

<a href="https://www.youtube.com/playlist?list=PL0ugL3aVdd-lCzn0rCKgtL-F6ECRM_N96
" target="_blank"><img src="GSuiteSetup.JPG" 
alt="YouTube Link to Complete Explanation" width="400" border="10" /></a>

